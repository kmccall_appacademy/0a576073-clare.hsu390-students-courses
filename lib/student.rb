
class Student
  attr_accessor :first_name, :last_name, :courses
  attr_reader :students, :course
  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    @name = @first_name + " " + @last_name
  end

  def enroll(course)
    @courses.any? do |course2|
      raise "Course Conflict" if course.conflicts_with?(course2)
    end
    @courses << course unless @courses.include?(course)
    course.students << self
  end

  def course_load
    course_load = Hash.new(0)
    self.courses.each do |course|
      course_load[course.department] += course.credits
    end
    course_load
  end

end
